﻿/*global require*/
// require in the complete Cesium object and reassign it globally.
// This is meant for use with the Almond loader.
require([
        'Cesium'
    ], function(
        Cesium) {
    'use strict';
    /*global self*/
    var scope = typeof window !== 'undefined' ? window : typeof self !== 'undefined' ? self : {};

    scope.Cesium = Cesium;
	var viewer = new Cesium.Viewer( 'cesiumContainerOne', {  
    imageryProvider : new  Cesium.ArcGisMapServerImageryProvider( {  
        url : 'http://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer'  
	} ), 
	fullscreenButton: false, 
    baseLayerPicker : false
} );
   var labelBall = viewer.imageryLayers.addImageryProvider(new  Cesium.ArcGisMapServerImageryProvider( {  
	url : 'http://services.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer' //'http://10.211.55.5:6080/arcgis/rest/services/10518/MapServer'//
} ));  
labelBall.show = false;
var terrainProvider = new Cesium.CesiumTerrainProvider( {  
    url : '//assets.agi.com/stk-terrain/world',  
    requestVertexNormals : true
});
viewer.terrainProvider = terrainProvider;
viewer.scene.globe.enableLighting = false;

	var scene = viewer.scene;

	// 17036014.2708 / (2 * Math.PI * 6378137.0) * 360,-3149543.7658 / (2 * Math.PI * 6378137.0) * 360


	var modelMatrix = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.035212,-27.365866, 100.0));  
	var modelMatrix1 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.037192,-27.513166, 10.0));//(153.444760,-28.560750, 10.0));  
	var modelMatrix2 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.049618,-27.514426, 10.0));//(153.444760,-28.560750, 10.0));  

	var modelMatrix3 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.048308,-27.50658, 10.0));//(153.444760,-28.560750, 10.0));  
	var modelMatrix4 = Cesium.Transforms.eastNorthUpToFixedFrame(  
		Cesium.Cartesian3.fromDegrees(153.041708,-27.51388, 10.0));//(153.444760,-28.560750, 10.0));  
var model = scene.primitives.add(Cesium.Model.fromGltf({  
    url : 'InClosed_Group_Set0.gltf',//'443InClosed.gltf',//'77.gltf',//如果为bgltf则为.bgltf  
	// color : new Cesium.Color(0/255, 255/255, 0/255, parseFloat(1.0)),
	modelMatrix : modelMatrix,  
	minimumPixelSize:35,
	maximumScale:35,
	 scale : 35,
	//  attributes:{
	// 	color: Cesium.ColorGeometryInstanceAttribute.fromColor(Cesium.Color.GREEN.withAlpha(1.0))
	// }
	
}));  

// var model1 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '1728InClosed.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix1,  
	
//      scale : 35
// }));
// var model2 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '4321InClosed.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix3,  
//      scale : 35
// }));

// var model3 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '10518InClosed.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix2,  
	
//      scale : 35
// }));
// var model4 = scene.primitives.add(Cesium.Model.fromGltf({  
//     url : '23386InClosed.gltf',//如果为bgltf则为.bgltf  
// 	modelMatrix : modelMatrix4,  
//      scale : 35
// })); 

viewer.camera.flyTo({  
    destination : Cesium.Cartesian3.fromDegrees(153.02283,-27.45642,16000.0)  
}); 


	function computeCircle(radius) {
    var positions = [];  
    for (var i = 0; i < 6; i++) { 
		var j = i*60;
        var radians = Cesium.Math.toRadians(j);  
        positions.push(new Cesium.Cartesian2(  
            radius * Math.cos(radians), radius * Math.sin(radians)));  
    }  
    return positions;  
	} 

	var canvas = scene.canvas; 
	var handler = new Cesium.ScreenSpaceEventHandler(scene.canvas); handler.setInputAction(function (movement) { 
   var cartesian = scene.camera.pickEllipsoid(movement.endPosition, ellipsoid); 
   
   var ellipsoid = scene.globe.ellipsoid; 
   if (cartesian) { //能获取，显示坐标
   var cartographic = ellipsoid.cartesianToCartographic(cartesian);
   var coords = '经度' + Cesium.Math.toDegrees(cartographic.longitude).toFixed(5) + ', ' + '纬度' + Cesium.Math.toDegrees(cartographic.latitude).toFixed(5) + '' + '高度' + Math.ceil(viewer.camera.positionCartographic.height); 
  //  $("#extent",self.parent.document).text(coords);
   var latlng = $("#extent",self.parent.document).text();
   if(latlng.length > 2){
	  var latlngArr = latlng.split(',');
	  viewer.camera.flyTo({  
		  destination : Cesium.Cartesian3.fromDegrees(latlngArr[0],latlngArr[1],latlngArr[2])  
	  }); 
   }
  
  
   } else { //不能获取不显示
	  }
  }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
  
  viewer.camera.moveEnd.addEventListener(function(){
	  // var camera = viewer.camera;
	  //获取视图中心点	
	  var result = viewer.camera.pickEllipsoid(new Cesium.Cartesian2 ( viewer.canvas.clientWidth /2 , viewer.canvas.clientHeight / 2));
	  var curPosition = Cesium.Ellipsoid.WGS84.cartesianToCartographic(result);
	  var lon = curPosition.longitude*180/Math.PI;
	  var lat = curPosition.latitude*180/Math.PI;
	  var height=scene.globe.ellipsoid.cartesianToCartographic(viewer.camera.position).height;//curPosition.height;
	  var cameraX = scene.camera.direction.x;
	  var cameraY = scene.camera.direction.y;
	  var cameraZ = scene.camera.direction.z;
	  $("#extent",self.parent.document).text(lon+","+lat+","+height+","+cameraX+","+cameraY+","+cameraZ);
  
  });



	function computeCircle(radius) {
    var positions = [];  
    for (var i = 0; i < 6; i++) { 
		var j = i*60;
        var radians = Cesium.Math.toRadians(j);  
        positions.push(new Cesium.Cartesian2(  
            radius * Math.cos(radians), radius * Math.sin(radians)));  
    }  
    return positions;  
	}  
	   

	function showShapeBall(viewer,ps,pointx,pointy,diameter)
	{
		var col;

		if(ps>=0)
		{
			col = Cesium.Color.LIGHTSLATEGREY;
		}
		
		if(ps>=0)
		{
			viewer.entities.add({ 
			name : 'show ball',
			position: Cesium.Cartesian3.fromDegrees(pointx, pointy,diameter), 
			ellipsoid : {  
			
			radii : new Cesium.Cartesian3(diameter, diameter, diameter),  
			material : col,   
			}
			});
		} 
	
		viewer.zoomTo(viewer.entities);
	}
	
	function showVec(){
		viewer.show = false;
		labelBall.show = true;
	}
	function showImg(){
		viewer.show = true;
		labelBall.show = false;
	}

	function showLabel(viewer,pointx,pointy,diameter,labelText,object){
		viewer.entities.add({
			name : 'label',
			position : Cesium.Cartesian3.fromDegrees(pointx, pointy,diameter),
			label : {
			  text : labelText,
			  font : '34pt monospace',
			  style: Cesium.LabelStyle.FILL_AND_OUTLINE,
			  outlineWidth : 2,
			  //水平位置
			  horizontalOrigin:Cesium.HorizontalOrigin.RIGHT,
			  //垂直位置
			  verticalOrigin : Cesium.VerticalOrigin.TOP,
			  //中心位置
			  pixelOffset : object
			}
		  });
		  viewer.zoomTo(viewer.entities);
	}
	
	

	// // if(label == 'A'){
	Cesium.loadJson('../../Test/BRTsTOPS.json').then(function(jsonData) {


		var high = 150;
		showLabel(viewer,153.017039,-27.474071,high,"1",new Cesium.Cartesian2(20, 0));
		showLabel(viewer,153.015945,-27.496553,10,"2",new Cesium.Cartesian2(-2, 0));
		showLabel(viewer,153.065253,-27.54555,high,"3",new Cesium.Cartesian2(40, 0));

		var p=0;
		var MAX_SUM_CO = 150;
		for(var i=0;i<jsonData.length;i++)
	
		{
			//debugger;
			var _tmpPush=[];
			var pointx = 0.0;
			var pointy = 0.0;
			var diameter = 100.0;
			if("Point" == jsonData[i].json_geometry.type)
			{
					_tmpPush=[];

					pointx=jsonData[i].json_geometry.coordinates[0];
					pointy=jsonData[i].json_geometry.coordinates[1];
					showShapeBall(viewer,MAX_SUM_CO,pointx,pointy,diameter);
			} 
			//debugger;
			
		}

		
	})




// }else 
$("#extent",self.parent.document).bind('DOMNodeInserted', function (e) {
	var latlng = $("#extent",self.parent.document).text();
   if(latlng.length > 2){
	  var latlngArr = latlng.split(',');
	  viewer.camera.flyTo({  
		  destination : Cesium.Cartesian3.fromDegrees(parseFloat(latlngArr[0]),parseFloat(latlngArr[1]),parseFloat(latlngArr[2]))
	  }); 
   }
});
	$("#layerVec",self.parent.document).click(function(){
		showVec();
	});
	$("#layerImg",self.parent.document).click(function(){
		showImg();
	});
	
	$('.cesium-viewer-bottom').remove();
	$('.cesium-viewer-toolbar').remove();
	$('.cesium-viewer-animationContainer').remove();
	$('.cesium-viewer-timelineContainer').remove();
}, undefined, true);
