﻿/*global require*/
// require in the complete Cesium object and reassign it globally.
// This is meant for use with the Almond loader.
require([
	'Cesium'
], function(
	Cesium) {
'use strict';
/*global self*/
var scope1 = typeof window !== 'undefined' ? window : typeof self !== 'undefined' ? self : {};

scope1.Cesium = Cesium;
var viewer1 = new Cesium.Viewer( 'number', {  
imageryProvider : new  Cesium.ArcGisMapServerImageryProvider( {  
	url : 'http://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer'  
} ),  


baseLayerPicker : false  
} );  
var terrainProvider1 = new Cesium.CesiumTerrainProvider( {  
url : '//assets.agi.com/stk-terrain/world',  
requestVertexNormals : true  
} );  
viewer1.terrainProvider = terrainProvider1;  
viewer1.scene.globe.enableLighting = false;  

var scene1 = viewer1.scene;
var handler1 = new Cesium.ScreenSpaceEventHandler(scene1.canvas);
handler1.setInputAction(function (movement) {
	scene1._noLoad=0;
	scene1._mayLoad=0;
	var pickedObjects = scene1.drillPick(movement.position);
	for(var s in pickedObjects)
	{
		//alert(pickedObjects[s].id);
	}
}, Cesium.ScreenSpaceEventType.LEFT_DOWN );
function computeCircle(radius) {
var positions = [];  
for (var i = 0; i < 6; i++) { 
	var j = i*60;
	var radians = Cesium.Math.toRadians(j);  
	positions.push(new Cesium.Cartesian2(  
		radius * Math.cos(radians), radius * Math.sin(radians)));  
}  
return positions;  
}  
   
function computeStar(arms, rOuter, rInner) {  
	var angle = Math.PI / arms;  
	var length = 2 * arms;  
	var positions = new Array(length);  
	for (var i = 0; i < length; i++) {  
		var r = (i % 2) === 0 ? rOuter : rInner;  
		positions[i] = new Cesium.Cartesian2(  
			Math.cos(i * angle) * r, Math.sin(i * angle) * r);  
	}  
	return positions;  
}  
function showShape(viewer,ps,resArr)
{
	var _tmpPush = resArr;
	var col;
	var widthp;
	if(ps<=443)
	{
		col=Cesium.Color.GREEN
		widthp=8;
	}
		
	if(ps>443 && ps<=1728)
	{
		col=Cesium.Color.GREEN;
		widthp=40;
	}
	if(ps>1728 && ps<=4321)
	{
		col=Cesium.Color.YELLOW;
		widthp=60;
	}
	if(ps>4321 && ps<=10517)
	{
		col=Cesium.Color.BLUE;
		widthp=90;
	}
	if(ps>10517)
	{
		col=Cesium.Color.RED;
		widthp=120;
	}
	if(ps>=0)
	{
		viewer.entities.add({  
		name : 'Red1 tube with rounded corners',
		polylineVolume : {
			positions : Cesium.Cartesian3.fromDegreesArray(  
				_tmpPush),
			shape : computeCircle(widthp),
			material : col
		}
		});
	}
	else
	{
		var redTube = viewer.entities.add({  
		name : 'Red2 tube with rounded corners',  
		polyline: {  
			positions : Cesium.Cartesian3.fromDegreesArray(  
				_tmpPush),  
			width : 2,
			material : col
		}  
		});
	}
	viewer.zoomTo(viewer.entities);
}


function showShapeBall(viewer,ps,pointx,pointy,diameter)
{
	var col;

	if(ps>=0)
	{
		col = Cesium.Color.LIGHTSLATEGREY;
	}
	
	if(ps>=0)
	{
		viewer.entities.add({ 
		name : 'show ball',
		position: Cesium.Cartesian3.fromDegrees(pointx, pointy,diameter), 
		ellipsoid : {  
		
		radii : new Cesium.Cartesian3(diameter, diameter, diameter),  
		material : col,   
		}
		});
	} 

	viewer.zoomTo(viewer.entities);
}

function showLabel(viewer,pointx,pointy,diameter,labelText,object){
	viewer.entities.add({
		name : 'label',
		position : Cesium.Cartesian3.fromDegrees(pointx, pointy,diameter),
		label : {
		  text : labelText,
		  font : '34pt monospace',
		  style: Cesium.LabelStyle.FILL_AND_OUTLINE,
		  outlineWidth : 2,
		  //水平位置
		  horizontalOrigin:Cesium.HorizontalOrigin.RIGHT,
		  //垂直位置
		  verticalOrigin : Cesium.VerticalOrigin.TOP,
		  //中心位置
		  pixelOffset : object
		}
	  });
	  viewer.zoomTo(viewer.entities);
}

Cesium.loadJson('../../Test/resultT.json').then(function(jsonData) {
	var p=0;
	for(var i=0;i<jsonData.length;i++)

	{
		//debugger;
		var _tmpPush=[];
		if("MultiLineString" == jsonData[i].json_geometry.type)
		{
			for(var j=0;j<jsonData[i].json_geometry.coordinates.length;j++)
			{
				_tmpPush=[];
				for(var k=0;k<jsonData[i].json_geometry.coordinates[j].length;k++)
				{
					_tmpPush.push(jsonData[i].json_geometry.coordinates[j][k][0]);
					_tmpPush.push(jsonData[i].json_geometry.coordinates[j][k][1]);
				}
				showShape(viewer1,jsonData[i].MAX_SUM_CO,_tmpPush);
				
			}
		}
		else
		{
			if("LineString" == jsonData[i].json_geometry.type)
			{
				for(var j=0;j<jsonData[i].json_geometry.coordinates.length;j++)
				{
					_tmpPush.push(jsonData[i].json_geometry.coordinates[j][0]);
					_tmpPush.push(jsonData[i].json_geometry.coordinates[j][1]);
				}	
			}
			showShape(viewer1,jsonData[i].MAX_SUM_CO,_tmpPush);
			
		}
		//debugger;
		
	}
	
})



var label = $(".number").text();
if(label == 'A'){
Cesium.loadJson('../../Test/BRTsTOPS.json').then(function(jsonData) {

	var high = 300;
	showLabel(viewer1,153.025839,-27.470371,high,"1",new Cesium.Cartesian2(20, 0));
	showLabel(viewer1,153.016945,-27.498043,10,"2",new Cesium.Cartesian2(-2, 0));
	showLabel(viewer1,153.064253,-27.54655,high,"3",new Cesium.Cartesian2(20, 0));
	var p=0;
	var MAX_SUM_CO = 150;
	for(var i=0;i<jsonData.length;i++)

	{
		//debugger;
		var _tmpPush=[];
		var pointx = 0.0;
		var pointy = 0.0;
		var diameter = 140.0;
		if("Point" == jsonData[i].json_geometry.type)
		{
				_tmpPush=[];

				pointx=jsonData[i].json_geometry.coordinates[0];
				pointy=jsonData[i].json_geometry.coordinates[1];
				showShapeBall(viewer1,MAX_SUM_CO,pointx,pointy,diameter);
		} 
		//debugger;
		
	}

	
})
}
// $('#number').innerText = 1;

// $('.cesium-viewer-bottom').remove();
// $('.cesium-viewer-toolbar').remove();
// $('.cesium-viewer-animationContainer').remove();
// $('.cesium-viewer-timelineContainer').remove();
}, undefined, true);
